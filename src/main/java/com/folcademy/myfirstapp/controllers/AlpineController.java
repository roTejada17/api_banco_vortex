package com.folcademy.myfirstapp.controllers;

import org.springframework.web.bind.annotation.*;

@RestController
public class AlpineController {
    @PostMapping ("/alpine")
    public String alpine (){
        return "Goodbye POST";
    }

    @GetMapping("/alpine")
    public String alpineget (){
        return "Goodbye GET METHOD";
    }
}
