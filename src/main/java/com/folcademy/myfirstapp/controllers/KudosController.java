package com.folcademy.myfirstapp.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KudosController {
    @PutMapping("/kudos")
    public String kudos(){
        return "World Put";
    }
}
