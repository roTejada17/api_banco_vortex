package com.folcademy.myfirstapp.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SenecaController {
    @RequestMapping("/")
    public String seneca() {
        return "Hello World";
    }
}
